CREATE TABLE `client` (
  `clientid` int(11) NOT NULL,
  `client_name` varchar(150) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `mailing_address` varchar(255) NOT NULL,
  `member_since` varchar(25) NOT NULL,
  `branch_of_registration` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `transaction` (
  `transactionid` int(11) NOT NULL,
  `clientid` int(11) NOT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `item_name` varchar(150) NOT NULL,
  `net_amount` float NOT NULL,
  `vat` float NOT NULL,
  `branch_location` varchar(250) NOT NULL,
  `timestamp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `client`
  ADD PRIMARY KEY (`clientid`);


ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transactionid`);

ALTER TABLE `client`
  MODIFY `clientid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `transaction`
  MODIFY `transactionid` int(11) NOT NULL AUTO_INCREMENT;
