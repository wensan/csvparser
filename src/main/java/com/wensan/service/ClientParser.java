package com.wensan.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.wensan.model.Client;
import com.wensan.util.Parser;

public class ClientParser extends Parser {
	private final static Logger logger = Logger.getLogger(ClientParser.class.getName());

	public static List<Client> parseClient(String clientCsvPath) {
		CSVParser parseList = parseCSV(clientCsvPath);
		List<Client> clients = new ArrayList<Client>();
		for (CSVRecord record : parseList) {
			Client client = new Client();
			client.setClientName(record.get("Client Name"));
			client.setContactNo(record.get("Contact No."));
			client.setMailingAddress(record.get("Mailing Address"));
			client.setMemberSince(record.get("Member Since"));
			client.setBranchOfRegistration(record.get("Branch of Registration"));
			clients.add(client);
		}

		return clients;
	}

	@SuppressWarnings("resource")
	public static void writeBadClients(List<Client> clients) {
		CSVFormat csvFormat = CSVFormat.DEFAULT.withHeader("Client Name", "Contact No.", "Mailing Address", "Member Since", "Branch of Registration");
		String path = "bad-record_clients_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".csv";

		try {
			BufferedWriter writer = Files.newBufferedWriter(Paths.get(path));

			CSVPrinter csvPrinter = new CSVPrinter(writer, csvFormat);

			for (Client client : clients) {
				csvPrinter.printRecord(Arrays.asList(client.getClientName(), client.getContactNo(), client.getMailingAddress(), client.getMemberSince(), client.getBranchOfRegistration()));
			}

			csvPrinter.flush();
		} catch(IOException e) {
			logger.log(Level.SEVERE, "Cannot write file: " + path);
			e.printStackTrace();
		}
	}
}
