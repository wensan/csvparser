package com.wensan.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.wensan.model.Transaction;
import com.wensan.util.Parser;

public class TransactionParser extends Parser {
	private final static Logger logger = Logger.getLogger(TransactionParser.class.getName());

	public static List<Transaction> parseTransaction(String transactionCsvPath) throws Exception {
		CSVParser parseList = parseCSV(transactionCsvPath);
		List<Transaction> transactions = new ArrayList<Transaction>();
		for (CSVRecord record : parseList) {
			Transaction transaction = new Transaction();
			transaction.setClientName(record.get("Client Name"));
			transaction.setPaymentMode(record.get("Payment Mode"));
			transaction.setItemName(record.get("Item Name"));
			transaction.setNetAmount(record.get("Net Amount"));
			transaction.setVat(record.get("VAT"));
			transaction.setBranchLocation(record.get("Branch Location"));
			transaction.setTimestamp(record.get("Timestamp"));
			transactions.add(transaction);
		}

		return transactions;
	}

	@SuppressWarnings("resource")
	public static void writeBadTransaction(List<Transaction> transactions) {
		CSVFormat csvFormat = CSVFormat.DEFAULT.withHeader("Client Name", "Payment Mode", "Item Name", "Net Amount", "VAT", "Branch Location", "Timestamp");
		String path = "bad-record_transactions_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".csv";

		try {
			BufferedWriter writer = Files.newBufferedWriter(Paths.get(path));

			CSVPrinter csvPrinter = new CSVPrinter(writer, csvFormat);
			for(Transaction transaction : transactions) {
				csvPrinter.printRecord(Arrays.asList(transaction.getClientName(), transaction.getPaymentMode(), transaction.getItemName(), transaction.getNetAmount(), transaction.getVat(), transaction.getBranchLocation(), transaction.getTimestamp()));
			}

			csvPrinter.flush();
		} catch(IOException e) {
			logger.log(Level.SEVERE, "Cannot write file: " + path);
			e.printStackTrace();
		}
	}
}
