package com.wensan.service;

import java.util.List;
import java.util.Map;

import com.wensan.dao.TransactionDAO;
import com.wensan.model.Transaction;

public class TransactionService {
	private TransactionDAO transactionDao;
	private static final TransactionService instance = new TransactionService();

	private TransactionService() {
		transactionDao = new TransactionDAO();
	}

	public static TransactionService getInstance() {
		return instance;
	}

	public Map<String, Object> insertTransaction(List<Transaction> transactions) {
		return transactionDao.insertTransaction(transactions);
	}
}
