package com.wensan.service;

import java.util.List;
import java.util.Map;

import com.wensan.dao.ClientDAO;
import com.wensan.model.Client;

public class ClientService {
	private ClientDAO clientDao;
	private static final ClientService instance = new ClientService();
	
	private ClientService() {
		clientDao = new ClientDAO();
	}
	
	public static ClientService getInstance() {
		return instance;
	}
	
	public Map<String, Object> insertClient(List<Client> clients) {
		return clientDao.insertClient(clients);
	}
}
