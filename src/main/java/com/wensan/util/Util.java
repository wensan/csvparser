package com.wensan.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Util {
	private final static Logger logger = Logger.getLogger(Util.class.getName());

	public Properties loadProperties(String path) {
		Properties props = new Properties();
		InputStream inputStream = null;
		try {
			inputStream = getClass().getClassLoader().getResourceAsStream(path);
			if (inputStream != null)
				props.load(inputStream);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Cannot open file: " + path);
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {}
			}
		}
		return props;
	}

	public static boolean isValidNumber(String num) {
		String decimalPattern = "([0-9]*)\\.([0-9]*)";
		return Pattern.matches(decimalPattern, num);
	}

	public static String mergeArray(int start, String[] strs) {
		StringBuffer merge = new StringBuffer();
		for (int i = start; i < strs.length; i++) {
			merge.append(" " + strs[i]);
		}
		return merge.toString();
	}

	public static void writeLogFile(String str, int received, int successfull, int size) {
		StringBuilder report = new StringBuilder();
		report.append("Log results for: " + str);
		report.append(System.lineSeparator());
		report.append(System.lineSeparator());
		report.append("# of records received: " + received);
		report.append(System.lineSeparator());
		report.append("# of records successful: " + successfull);
		report.append(System.lineSeparator());
		report.append("# of records failed: " + (size - successfull));
		report.append(System.lineSeparator());

		BufferedWriter bw = null;
		FileWriter fw = null;
		String filePath = "logfile_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".txt";

		try {
			fw = new FileWriter(filePath);
			bw = new BufferedWriter(fw);
			bw.write(report.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
