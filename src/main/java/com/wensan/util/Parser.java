package com.wensan.util;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

public class Parser {
	private final static Logger logger = Logger.getLogger(Parser.class.getName());
	public static CSVParser parseCSV(String csvPath) {
		try {
			Reader reader = Files.newBufferedReader(Paths.get(csvPath));
			CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withTrim());
			return csvParser;
		} catch(IOException e) {
			logger.log(Level.SEVERE, "Cannot open file: " + csvPath);
			e.printStackTrace();
			return null;
		}
	}
}
