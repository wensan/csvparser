package com.wensan.model;


public class Transaction {
	private String clientName;
	private String paymentMode;
	private String itemName;
	private String netAmount;
	private String vat;
	private String branchLocation;
	private String timestamp;

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getBranchLocation() {
		return branchLocation;
	}

	public void setBranchLocation(String branchLocation) {
		this.branchLocation = branchLocation;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Transaction [clientName=" + clientName + ", paymentMode=" + paymentMode + ", itemName=" + itemName
				+ ", netAmount=" + netAmount + ", vat=" + vat + ", branchLocation=" + branchLocation + ", timestamp="
				+ timestamp + "]";
	}
}
