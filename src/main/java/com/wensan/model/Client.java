package com.wensan.model;

public class Client {
	private String clientName;
	private String contactNo;
	private String mailingAddress;
	private String memberSince;
	private String branchOfRegistration;

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public String getMemberSince() {
		return memberSince;
	}

	public void setMemberSince(String memberSince) {
		this.memberSince = memberSince;
	}

	public String getBranchOfRegistration() {
		return branchOfRegistration;
	}

	public void setBranchOfRegistration(String branchOfRegistration) {
		this.branchOfRegistration = branchOfRegistration;
	}

	@Override
	public String toString() {
		return "Client [clientName=" + clientName + ", contactNo=" + contactNo + ", mailingAddress=" + mailingAddress
				+ ", memberSince=" + memberSince + ", branchOfRegistration=" + branchOfRegistration + "]";
	}
}
