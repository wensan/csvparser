package com.wensan;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.wensan.model.Client;
import com.wensan.model.Transaction;
import com.wensan.service.ClientParser;
import com.wensan.service.ClientService;
import com.wensan.service.TransactionParser;
import com.wensan.service.TransactionService;
import com.wensan.util.Csv;
import com.wensan.util.Util;

public class App {
	private static final Logger logger = Logger.getLogger(App.class.getName());
	private static String process;
	private static String csvPath;

    @SuppressWarnings("unchecked")
	public static void main( String[] args ) throws Exception {
    	try {
    		for(int i = 0; i < args.length; i++) {
    			if (args[i] != null) {
    				if (args[i].split("=")[0].equals("--process") && args[i].split("=")[1] != null) {
    					process = args[i].split("=")[1];
					} else if (args[i].split("=")[0].equals("--csvPath") && args[i].split("=")[1] != null) {
						if ((i+1) < args.length) {
							csvPath = args[i].split("=")[1] + Util.mergeArray(i+1, args);
						} else {
							csvPath = args[i].split("=")[1];
						}
					}
    			}
    		}
    	} catch(ArrayIndexOutOfBoundsException e) {
    		logger.log(Level.WARNING, "Invalid Parameters.");
			System.exit(0);
    	}

    	if (process != null && process.equals(Csv.CLIENT.name().toLowerCase())) {
    		if (csvPath != null) {
    			List<Client> clients = ClientParser.parseClient(csvPath);
    			Map<String, Object> result = ClientService.getInstance().insertClient(clients);
    			List<Client> badClients = (List<Client>) result.get("badClient");
    			if (badClients != null)
    				ClientParser.writeBadClients(badClients);
    			Util.writeLogFile("Client", (Integer) result.get("receive"), (Integer) result.get("success"), clients.size());
    		} else {
    			logger.log(Level.WARNING, "Invalid value of csv path.");
    		}
    	} else if(process != null && process.equals(Csv.TRANSACTION.name().toLowerCase())) {
    		if (csvPath != null) {
    			List<Transaction> transactions = TransactionParser.parseTransaction(csvPath);
    			Map<String, Object> result = TransactionService.getInstance().insertTransaction(transactions);
    			List<Transaction> badTransactions = (List<Transaction>) result.get("badTransaction");
    			if (badTransactions != null)
    				TransactionParser.writeBadTransaction(badTransactions);

    			Util.writeLogFile("Transaction", (Integer) result.get("receive"), (Integer) result.get("success"), transactions.size());
    		} else {
    			logger.log(Level.WARNING, "Invalid value for csv path.");
    		}
    	} else {
    		logger.log(Level.WARNING, "Invalid value for process.");
    	}
    }
}
