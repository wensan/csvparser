package com.wensan.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wensan.model.Client;
import com.wensan.util.DB;
import com.wensan.util.Util;

public class ClientDAO extends DB {
	private PreparedStatement pre = null;
	
	public ClientDAO() {
		super(new Util());
	}
	
	public Map<String, Object> insertClient(List<Client> clients) {
		String sql = "INSERT INTO client(client_name, contact_no, mailing_address, member_since, branch_of_registration) ";
			   sql += "VALUES(?, ?, ?, ?, ?)";
		
		pre = getPrep(sql);
		setAutocommit(false);
		
		try {
			String sqlCheckExist = "SELECT * FROM client WHERE client_name = ? AND contact_no = ?";
			List<Client> badClient = new ArrayList<Client>(); 
			for (Client client : clients) {
				ResultSet rs = selectQuery(sqlCheckExist, client.getClientName(), client.getContactNo());
				if (rs.next()) {
					badClient.add(client);
				} else {
					pre.setString(1, client.getClientName());
					pre.setString(2, client.getContactNo());
					pre.setString(3, client.getMailingAddress());
					pre.setString(4, client.getMemberSince());
					pre.setString(5, client.getBranchOfRegistration());
					pre.addBatch();
				}				
			}
			int[] insertedRows = pre.executeBatch();			
			commit();
			
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("badClient", badClient);
			result.put("success", insertedRows.length);
			result.put("receive", clients.size());
			return result;
		} catch(SQLException e) {
			rollback();
			throw new RuntimeException("An error encountered.", e);			
		}
	}
}
