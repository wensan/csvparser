package com.wensan.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wensan.model.Transaction;
import com.wensan.util.DB;
import com.wensan.util.Util;

public class TransactionDAO extends DB {
	private PreparedStatement pre = null;

	public TransactionDAO() {
		super(new Util());
	}

	public Map<String, Object> insertTransaction(List<Transaction> transactions) {
		String sql = "INSERT INTO transaction(clientid, payment_mode, item_name, net_amount, vat, branch_location, timestamp) ";
			   sql += "VALUES(?, ?, ?, ?, ?, ?, ?)";

	    pre = getPrep(sql);
		setAutocommit(false);
		try {
			String sqlClient = "SELECT clientid FROM client WHERE client_name = ?";
			List<Transaction> badTransaction = new ArrayList<Transaction>();
			for (Transaction transaction : transactions) {
				ResultSet rs = selectQuery(sqlClient, transaction.getClientName());
				if (rs.next()) {
					String sqlTransaction = "SELECT * FROM transaction WHERE clientid = ? AND branch_location = ? AND timestamp = ?";
					ResultSet rsTran = selectQuery(sqlTransaction, rs.getInt("clientid"), transaction.getBranchLocation(), transaction.getTimestamp());
					if (rsTran.next()) {
						badTransaction.add(transaction);
					} else {
						if (Util.isValidNumber(transaction.getNetAmount().replace(",", "")) && Util.isValidNumber(transaction.getVat().replace(",", ""))) {
							pre.setInt(1, rs.getInt("clientid"));
							pre.setString(2, transaction.getPaymentMode());
							pre.setString(3, transaction.getItemName());
							pre.setFloat(4, Float.parseFloat(transaction.getNetAmount().replace(",", "")));
							pre.setFloat(5, Float.parseFloat(transaction.getVat().replace(",", "")));
							pre.setString(6, transaction.getBranchLocation());
							pre.setString(7, transaction.getTimestamp());
							pre.addBatch();
						} else {
							badTransaction.add(transaction);
						}
					}
				} else {
					badTransaction.add(transaction);
				}
			}
			int[] insertedRows = pre.executeBatch();
			commit();

			Map<String, Object> result = new HashMap<String, Object>();
			result.put("badTransaction", badTransaction);
			result.put("success", insertedRows.length);
			result.put("receive", transactions.size());

			return result;
		} catch(SQLException e) {
			rollback();
			throw new RuntimeException("An error encountered.", e);
		}
	}
}
