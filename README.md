# CSV Parser Challenge
Requirements:

* Create a Java application that will process 2 input CSV formatted files and parse their
contents accordingly:
	* Input files:
		* CodingChallenge - Clients.csv
		* CodingChallenge - Transactions.csv
	* Lines are separated by CRLF or LF control characters.
	* Top line contains field names for the columns
	* Values that are meant to contain commas will be double quoted
	* Create a table for each file to contain the column values
		* Save the DDL commands in the readme file to be created later.
		* When creating Clients table, make sure to add primary key autoincrementing
column named clientid
		* When creating Transactions table:
			* make sure to add primary key auto-incrementing column named
transactionid
			* include new column named clientid after column Client Name
		* Lines with the correct number of columns are to be saved in their respective tables
		* When processing Transactions input file
			* search for client name in Clients table
			* Get clientid retrieved from search, and save it with the transaction record
		* Invalid lines should be saved in respective files:
			* bad-record_clients_timestamp.csv
			* bad-record_transactions_timestamp.csv
		* After the file is processed, write statistics to logfile_timestamp.txt
			* clients
				* no of records received
				* no of records successful
				* no of records failed
			* transactions
				* no of records received
				* no of records successful
				* no of records failed
* Make sure the application is optimized for processing very large files
* Utilizing maven and open source libraries is encouraged
* In the folder containing your source code, create a README file that will contain the
sections listed below. The README may be composed with a markup language usable
in bitbucket.
* Create a bitbucket account and repository to which you’ll upload your source code.

## To compile and run:
* The project is using MySQL
	* Create database and set the database name in ```main/resources/config.properties``` file found in resources folder
		* ``` db.url=jdbc:mysql://localhost:3306/ms3```
		* run the DDL below			
		
			CREATE TABLE 'client' ('clientid' int(11) NOT NULL,
				'client_name' varchar(150) NOT NULL, 
				'contact_no' varchar(50) NOT NULL, 
				'mailing_address' varchar(255) NOT NULL, 
				'member_since' varchar(25) NOT NULL, 
				'branch_of_registration' varchar(150) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
            CREATE TABLE 'transaction' ('transactionid' int(11) NOT NULL, 
				'clientid' int(11) NOT NULL, 
				'payment_mode' varchar(50) NOT NULL, 
				'item_name' varchar(150) NOT NULL, 
				'net_amount' float NOT NULL, 
				'vat' float NOT NULL, 
				'branch_location' varchar(250) NOT NULL, 
				'timestamp' varchar(100) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
			ALTER TABLE 'client' ADD PRIMARY KEY ('clientid');
			
			ALTER TABLE 'transaction' ADD PRIMARY KEY ('transactionid');
			
			ALTER TABLE 'client' MODIFY 'clientid' int(11) NOT NULL AUTO_INCREMENT;
			
			ALTER TABLE 'transaction' MODIFY 'transactionid' int(11) NOT NULL AUTO_INCREMENT;
			
* Inside the project folder, run: "mvn clean compile assembly:single"
	* ``` mvn clean compile assembly:single ```
* This will create a jar file with dependencies inside the target folder ```target/csvparser-jar-with-dependencies.jar ```
* You can rename the jar file ex. "csv.jar"
* To run, type: ``` java -cp  "{name of jar file}" com.wensan.App --process={type of csv} --csvPath={full path of csv file match with the type}```
* ex:
	```
	java -cp "csv.jar" com.wensan.App --process=client --csvPath=C:/camotechq/csvparser/CodingChallenge - Clients.csv
    ```
    * argument params values
    	* --process = 'client or transaction'
    	* --csvPath = "the complete path of the csv file match with the process type above"
    	* com.wensan.App = this is the main
 
 
* Project Description
	* Built on maven
	* Utilities:
		* Database main class is in DB.java
		* Main parser is in Parser.java, using apache common csv library
		* Settings for DB is in config.properties file
		* Util.java contains getting properties file & wrinting the log txt
	* DB transactions are found in Dao folder
	* Flow:
		* If parameters are valid, it will call the generic parser to parse the csv and add to model accordingly, it will check with DB first if data are valid before writing, once DB interaction is done, it will write the reports, same folder with the jar file.

